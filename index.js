function tinhLuong() {
  var txtLuongngay = document.getElementById("txt-tien-luong-ngay").value * 1;
  var txtNgaylam = document.getElementById("txt-ngay-lam").value * 1;

  var tienLuong = txtLuongngay * txtNgaylam;

  document.getElementById(
    "result"
  ).innerHTML = `Tiền lương nhận được là ${tienLuong}$;`;
}
function tinhTBC() {
  var so1 = document.getElementById("txt-so-1").value * 1;
  var so2 = document.getElementById("txt-so-2").value * 1;
  var so3 = document.getElementById("txt-so-3").value * 1;
  var so4 = document.getElementById("txt-so-4").value * 1;
  var so5 = document.getElementById("txt-so-5").value * 1;

  var TBC = (so1 + so2 + so3 + so4 + so5) / 5;

  document.getElementById(
    "resultBai2"
  ).innerText = `Trung bình cộng bằng ${TBC}`;
}
function quyDoi() {
  var tienUSD = document.getElementById("txt-tienUSD").value * 1;

  document.getElementById("resultBai3").innerText = new Intl.NumberFormat(
    "vn-VN",
    { style: "currency", currency: "VND" }
  ).format(tienUSD * 23500);
}
function chuvi_dientich() {
  var chieuDai = document.getElementById("txt-chieu-dai").value * 1;
  var chieuRong = document.getElementById("txt-chieu-rong").value * 1;

  var dienTich = chieuDai * chieuRong;
  var chuVi = (chieuDai + chieuRong) * 2;

  document.getElementById(
    "resultBai4"
  ).innerText = `Chu vi = ${chuVi}, Diện tích = ${dienTich}`;
}
function tong() {
  var so = document.getElementById("txt-so").value * 1;

  var soDonvi = so % 10;
  var soHangchuc = Math.floor(so / 10);
  var tong = soDonvi + soHangchuc;

  document.getElementById("resultBai5").innerText = tong;
}
function chuyenBai1() {
  console.log("bai1");
  document.getElementById("form_input_bai1").style.display = "block";
  document.getElementById("form_input_bai2").style.display = "none";
  document.getElementById("form_input_bai3").style.display = "none";
  document.getElementById("form_input_bai4").style.display = "none";
  document.getElementById("form_input_bai5").style.display = "none";
}
function chuyenBai2() {
  console.log("bai2");
  document.getElementById("form_input_bai1").style.display = "none";
  document.getElementById("form_input_bai2").style.display = "block";
  document.getElementById("form_input_bai3").style.display = "none";
  document.getElementById("form_input_bai4").style.display = "none";
  document.getElementById("form_input_bai5").style.display = "none";
}
function chuyenBai3() {
  document.getElementById("form_input_bai1").style.display = "none";
  document.getElementById("form_input_bai2").style.display = "none";
  document.getElementById("form_input_bai3").style.display = "block";
  document.getElementById("form_input_bai4").style.display = "none";
  document.getElementById("form_input_bai5").style.display = "none";
}
function chuyenBai4() {
  document.getElementById("form_input_bai1").style.display = "none";
  document.getElementById("form_input_bai2").style.display = "none";
  document.getElementById("form_input_bai3").style.display = "none";
  document.getElementById("form_input_bai4").style.display = "block";
  document.getElementById("form_input_bai5").style.display = "none";
}
function chuyenBai5() {
  document.getElementById("form_input_bai1").style.display = "none";
  document.getElementById("form_input_bai2").style.display = "none";
  document.getElementById("form_input_bai3").style.display = "none";
  document.getElementById("form_input_bai4").style.display = "none";
  document.getElementById("form_input_bai5").style.display = "block";
}
